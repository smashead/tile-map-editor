/*EDITOR 0.11.2
//
  TODO:
	- dodac obsluge warstw
	---------------------------------
	@- doda� flood fill
//
  BUGI
	- czasami program wysypuje sie przy wczytywaniu mapy, nie wiem dlaczego (nie obserwuj� tego ostatnio w 0.9)
	
	- A/D przelaczaja na jakis kafelek (najblizszy) na widocznej palecie - bez sensu
	- jak si� wybierze kolor ze starej palety i wybierzesz now� to rysuj�c
	  dalej u�ywa Ci tego pierwszego <- dalej tak jest czasami
	- jak sie przewinie palete i potem zaladuje nowa to ma zla numeracje
	- tlo sie nie przesuwa

	oba bledy mozna naprawic ustawiajac po zaladowaniu palety wszystko na 0, picker tez
	
//
  CHANGELOG:
  0.1
    - dodana obs�uga prostok�tnych palet
	- wy�wietla przesuni�t� palet�, ale jedynie po wprowadzeniu przesuni�cia na sztywno w kodzie
  0.2
	- przesuwanie palet dzia�a w 100%
	- ograniczone wybieranie kafelk�w z palety mniejszej ni� 28
	- dodano pobieranie kafelka z mapy za pomoc� PPM (picker)
	- �adowanie podanej palety z pliku (trzeba poda� rozszerzenie)
  0.3
	- dodano mo�liwo�� wczytania palety o r�nych rozmiarach kafelk�w (podaje si� je przy wczytywaniu)
	- BUGFIX naprawiono zapisywanie si� map z podw�jnym rozszerzeniem po ich otworzeniu i zapisaniu (np. mapa.txt.txt)
	- dodany przycisk z info o programie
	- dodane ograniczenie dla tworzenia map wi�kszych ni� 400x400
	- dane zapakowane do pliku .dat
  0.4
	- dodano otwieranie map za pomoc� okna dialogowego
	- dodano �adowanie palet za pomoc� okna dialogowego, ale kosztem u�ywania plik�w .dat
  0.5
	- przywr�cowne u�ywanie plik�w .dat
	- BUGFIX nie mo�na ju� poda� rozmiaru kafelka wiekszego od rozmiar�w wczytywanej palety
	- naprawione sejwy, u�ywaj� okna dialogowego
	- mapa jest przerysowywana po zmianie palety
	- domy�lna warto�� pustego pola na mapie to teraz '-1' zamiast '0'
	- dodano obs�ug� klawiatury: Q/E - poprzedni/nast�pny z aktualnie widocznych na palecie kafelk�w
	- poprawiono czcionk� i zmieniono kursor (na lepsze?)
  0.6
	- malowanie zmienione na ci�g�e
	- BUGFIX program nie wy��cza si� ani nie wysypuje po anulowaniu wyboru palety (pow�d b�edu to usuni�cie ca�ego dialogu po anulowaniu)
	- zablokowana mo�liwo�� podniesienia kafelka '-1' (prawie jak MissingNo :D)
	- przy sejwach mapa nadpisuje si�, je�li jest otwarta ca�y czas ta sama
	- wy�wietlanie �cie�ki dost�pu do otwartej mapy (pierwsze 41 znak�w)
	- dodane/zmienione sterowanie palet� z klawiatury: W/S/A/D
  0.7
	- dodane GUI (podanie rozmiaru mapy, podanie rozmiaru kafelk�w)
	- BUGFIX po otwarciu mapy, ale braku wybranego wczesniej kafelka do malowania, mozna ju� pobra� kafelek od razu z mapy (pow�d: brak flagi)
	- BUGFIX po wybraniu kafelka z nowo-za�adowanej palety, zmienia si� kafelek "trzymany" przez kursor
	- BUGFIX po wyjechaniu kursorem poza okno przy fladze 'paint == true' (malowanie ci�g�e) program ju� nie wysypuje si�
	- zablokowano wy�wietlanie nazwy za�adowanej mapy (wkurzaj�ce)
  0.8
	- BUGFIX Naprawione: "Nie mo�na pobra� przez PPM kafelka, kt�ry nie jest aktualnie wy�wietlany z prawej strony.
		Tzn. pobierany jest numer kafelka i jest on wpisywany w map�, ale nie rysowany
		i dopiero po przesuni�ciu palety (nawet o 1) jest on poprawnie rysowany na mapie."
	- Dodano komunikaty graficzne: o nadpisaniu mapy, o potrzebie za�adowanie palety, �cie�k� do pliku (klawisz T).
  0.9
	- BUGFIX poprawione skolowanie map o wymiarach kafelk�w niepodzielnych przez 10, tzn. 16, 32, 48, itd.
	- dodano wiadomo�� z ostrze�eniem po podaniu wielko�ci kafelka wi�kszej ni� ca�ej bitmapy
	- teraz za pomoc� klawiszy A/D mo�na prze��cza� kafelki z ca�ej palety, a nie tylko z widocznej cz�ci
	- dodano przewijanie palety za pomoc� rolki myszy
  0.10
	- dodano narz�dzie rysowania prostok�t�w
  0.11.1
	- dodano narz�dzie rysowania linii
	- BUGFIX naprawiony b��d przy �adowaniu palety (z�y nr i wygl�d kafelka) przez zerowanie podniesionego kafelka
	- zmieniony zosta� mechanizm wy�wietlania mapy - teraz zajmuje duzo mniej RAMu (mog� ujawni� si� miejscami b��dy z tym zwi�zane)
	- w zwi�zku z powy�szym zosta�o usuni�te denerwuj�ce migni�cie podczas rysowania linii i prostok�t�w
	- dodano opcj� powi�kszenia/zmniejszenia aktualnie edytowanej mapy
	- dodany prze��cznik warstw (tylko grafika i pocz�tkowy mechanizm)
  0.11.2
	- zlikwidowane miganie ekranu podczas przewijania palety (niepotrzebne przerysowanie mapy w funkcji rysuj�cej palet�)
//
*/
#include <stdio.h>
#include <stdlib.h>
#include <allegro5\allegro.h>
#include <allegro5\allegro_image.h>
#include <allegro5\allegro_native_dialog.h>
#include <allegro5\allegro_font.h>
#include <allegro5\allegro_ttf.h>
#include <allegro5\allegro_physfs.h>
#include <physfs.h>

#include "editor.h"

//-------------------------------------------------------------// INIT
int init(void)
{
	if (!al_init())	{
		fprintf(stderr, "Blad inicjalizacji Allegro!\n", 1);
		return -1;}
	//-------------------------------------------------------------//
	if (!al_init_image_addon()) {
		fprintf(stderr, "Blad inicjalizacji Allegro image add-on!\n", 1);
		getchar();
		return -1;}
	//-------------------------------------------------------------//
	if (!al_install_mouse()) {
		fprintf(stderr, "ERROR: Cannot install mouse!\n", 1);
		getchar();
		return -1;}
	//-------------------------------------------------------------//
	if (!al_install_keyboard()) {
		fprintf(stderr, "ERROR: Cannot install keyboard!\n", 1);
		getchar();
		return -1;}
	//-------------------------------------------------------------//
	display = al_create_display(40*TILE_SIZE, 30*TILE_SIZE);
	if (!display) {
		fprintf(stderr, "ERROR: Cannot create display!\n", 1);
		al_destroy_timer(timer);
		return -1;}
	window_icon = al_load_bitmap("ico.png");
	al_set_display_icon(display, window_icon);
	al_set_window_title(display, "Tile Map Editor 0.11.2");
	//-------------------------------------------------------------//
	timer = al_create_timer(1.0/FPS);
	if (!timer)	{
		fprintf(stderr, "Blad tworzenia timer!\n", 1);
		al_destroy_display(display);
		return -1;}
	//-------------------------------------------------------------//
	event_queue = al_create_event_queue();
	if (!event_queue) {
		fprintf(stderr, "Blad tworzenia event_queue!\n", 1);
		al_destroy_display(display);
		al_destroy_timer(timer);
		return -1;}
	//-------------------------------------------------------------//
	w_square = al_create_bitmap(TILE_SIZE, TILE_SIZE);
	if (!w_square) {
		fprintf(stderr, "ERROR: Cannot create \"w_square\" bitmap!\n", 1);
		al_destroy_display(display);
		al_destroy_timer(timer);
		al_destroy_event_queue(event_queue);
		return 1;}
	//-------------------------------------------------------------//
	current_tile = al_create_bitmap(TILE_SIZE, TILE_SIZE);
	if (!current_tile) {
		fprintf(stderr, "ERROR: Cannot create \"current_tile\" bitmap!\n", 1);
		al_destroy_display(display);
		al_destroy_timer(timer);
		al_destroy_event_queue(event_queue);
		return -1;}
	//-------------------------------------------------------------//
	menu_tile = al_load_bitmap("menu.png");
	if (!menu_tile) {
		fprintf(stderr, "ERROR: Cannot create \"menu_tile\" bitmap!\n", 1);
		al_destroy_display(display);
		al_destroy_timer(timer);
		al_destroy_event_queue(event_queue);
		return -1;}
	//-------------------------------------------------------------//
	menu_buttons = al_load_bitmap("buttons.png");
	if (!menu_tile) {
		fprintf(stderr, "ERROR: Cannot create \"menu_buttons\" bitmap!\n", 1);
		al_destroy_display(display);
		al_destroy_timer(timer);
		al_destroy_event_queue(event_queue);
		return -1;}
	//-------------------------------------------------------------//
	app_cursor = al_load_bitmap("appcursor.bmp");
	if (!app_cursor) {
		fprintf(stderr, "ERROR: Cannot create \"app_cursor\" bitmap!\n", 1);
		al_destroy_display(display);
		al_destroy_timer(timer);
		al_destroy_event_queue(event_queue);
		getchar();
		return -1;}
	//-------------------------------------------------------------//
	al_init_font_addon();
	if (!al_init_ttf_addon()) {
		fprintf(stderr, "Blad inicjalizacji Allegro image add-on!\n", 1);
		al_destroy_display(display);
		al_destroy_timer(timer);
		al_destroy_event_queue(event_queue);
		getchar();
		return -1;}
	//-------------------------------------------------------------//}
	file_opener = al_create_native_file_dialog(".", "Open your map file", "*.txt", ALLEGRO_FILECHOOSER_FILE_MUST_EXIST);
	if (!file_opener) {
		fprintf(stderr, "ERROR: Cannot create file_opener dialog\n", 1);
		al_destroy_display(display);
		al_destroy_timer(timer);
		al_destroy_event_queue(event_queue);
		getchar();
		return -1;}
	//-------------------------------------------------------------//}
	palette_opener = al_create_native_file_dialog(".//", "Open your palette bitmap file", "*.bmp;*.png;*.gif;*.jpg", ALLEGRO_FILECHOOSER_FILE_MUST_EXIST);
	if (!file_opener) {
		fprintf(stderr, "ERROR: Cannot create palette_opener dialog\n", 1);
		al_destroy_display(display);
		al_destroy_timer(timer);
		al_destroy_event_queue(event_queue);
		al_destroy_native_file_dialog(file_opener);
		getchar();
		return -1;}
	//-------------------------------------------------------------//}
	file_saver = al_create_native_file_dialog(".//", "Save your map file", "*.txt", ALLEGRO_FILECHOOSER_SAVE);
	if (!file_opener) {
		fprintf(stderr, "ERROR: Cannot create file_saver dialog\n", 1);
		al_destroy_display(display);
		al_destroy_timer(timer);
		al_destroy_event_queue(event_queue);
		al_destroy_native_file_dialog(file_opener);
		al_destroy_native_file_dialog(palette_opener);
		getchar();
		return -1;}
	//-------------------------------------------------------------//
	printf("Initialization successful!\n\n");

	al_register_event_source(event_queue, al_get_display_event_source(display));
	al_register_event_source(event_queue, al_get_timer_event_source(timer));
	al_register_event_source(event_queue, al_get_mouse_event_source());
	al_register_event_source(event_queue, al_get_keyboard_event_source());
	al_start_timer(timer);
	//
	// Kursor
	//
	al_convert_mask_to_alpha(app_cursor, al_map_rgb(255, 0, 255));
	cursor = al_create_mouse_cursor(app_cursor, 6, 6);
	al_set_mouse_cursor(display, cursor);
	//
	// Czcionki
	//
	//font = al_load_font("orangekid.ttf", 8, 0);
	font = al_load_font("biocomc.ttf", 8, 0);
	fontbig = al_load_font("orangekid.ttf", 20, 0);
	//
	// Kolory kafelk�w t�a
	//
	al_set_target_bitmap(current_tile);
	al_clear_to_color(al_map_rgba(64, 64, 255, 127));
	al_set_target_backbuffer(display);
	al_clear_to_color(al_map_rgb(0, 0, 0));

	return 0;
}


//-------------------------------------------------------------// DESTROY
int destroy(void)
{
	// Niszczenie obiekt�w Allegro.
	// Ta lista powinna by� wci�� aktualizowana,
	// w przeciwnym wypadku, program mo�e wysypa�
	// si� na wyj�ciu albo/i zostawia� �mieci w pami�ci.
	//
	al_destroy_native_file_dialog(palette_opener);
	al_destroy_native_file_dialog(file_opener);
	al_destroy_native_file_dialog(file_saver);
	al_destroy_event_queue(event_queue);
	al_destroy_timer(timer);
	al_destroy_font(font);
	al_destroy_font(fontbig);
	al_destroy_mouse_cursor(cursor);
	al_destroy_bitmap(buffer);
	al_destroy_bitmap(w_square);
	al_destroy_bitmap(current_tile);
	al_destroy_bitmap(palette_menu_buffer);
	al_destroy_bitmap(palette_buffer);
	al_destroy_bitmap(menu_buffer);
	al_destroy_bitmap(menu_tile);
	al_destroy_bitmap(menu_buttons);
	al_destroy_bitmap(app_cursor);
	al_destroy_bitmap(palette);
	al_destroy_bitmap(palette_s);
	al_destroy_bitmap(window_icon);
	al_destroy_display(display);

	return 0;
}


//-------------------------------------------------------------// MAP BACKGROUND
int map_back(int me_w, int me_h)
{
	al_set_target_bitmap(w_square);
	al_clear_to_color(al_map_rgb(64, 64, 64));

	if (me_w > DW) me_w = DW;
	if (me_h > DH) me_h = DH;

	if (buffer)
		al_destroy_bitmap(buffer);
	buffer = al_create_bitmap(me_w*TILE_SIZE, me_h*TILE_SIZE);
	al_set_target_bitmap(buffer);
	al_clear_to_color(al_map_rgb(32, 32, 32));
	for (int me_x = 0; me_x < me_w; ++me_x) {
		for (int me_y = 0; me_y < me_h; ++me_y) {
			if ((me_x + me_y)%2) {
				al_draw_bitmap(w_square, me_x*TILE_SIZE, me_y*TILE_SIZE, 0);
			}
		}
	}
	return 0;
}


//-------------------------------------------------------------// Rysuj PALETTE MENU
int draw_palette_menu(void)
{
	palette_menu_buffer = al_create_bitmap(3*TILE_SIZE, 30*TILE_SIZE);
	al_set_target_bitmap(palette_menu_buffer);
	al_clear_to_color(al_map_rgb(64, 127, 64));
	for (int pmenu_x = 0; pmenu_x < 3; ++pmenu_x) {
		for (int pmenu_y = 0; pmenu_y < 30; ++pmenu_y) {
			al_draw_bitmap_region(menu_tile, (mmenu[pmenu_y][pmenu_x]%4)*TILE_SIZE, (mmenu[pmenu_y][pmenu_x]/4)*TILE_SIZE, TILE_SIZE, TILE_SIZE, pmenu_x*TILE_SIZE, pmenu_y*TILE_SIZE, 0);
		}
	}
	return 0;
}
//-------------------------------------------------------------// Rysuj MENU
int draw_menu(void)
{
	menu_buffer = al_create_bitmap(37*TILE_SIZE, 3*TILE_SIZE);
	al_set_target_bitmap(menu_buffer);
	al_clear_to_color(al_map_rgb(64, 64, 127));
	for (int x = 0; x < 37; ++x) {
		for (int y = 0; y < 3; ++y) {
			al_draw_bitmap_region(menu_tile, (dmenu[y][x]%4)*TILE_SIZE, (dmenu[y][x]/4)*TILE_SIZE, TILE_SIZE, TILE_SIZE, x*TILE_SIZE, y*TILE_SIZE, 0);
		}
	}
	return 0;
}


//-------------------------------------------------------------// Rysuj przyciski menu
int draw_button(int sx, int sy, int dx, int dy, bool state)
{
	// Przyjmuje jeden z 2 stan�w:
	// false = normalny przycisk
	// true = pod�wietlony przycisk
	if (state == false)
		al_draw_bitmap_region(menu_buttons, sx*TILE_SIZE,		sy*TILE_SIZE, TILE_SIZE, TILE_SIZE, dx*TILE_SIZE, dy*TILE_SIZE, 0);
	else if (state == true)
		al_draw_bitmap_region(menu_buttons, (sx+1)*TILE_SIZE,	sy*TILE_SIZE, TILE_SIZE, TILE_SIZE, dx*TILE_SIZE, dy*TILE_SIZE, 0);
	else {
		printf("ERROR: new_button cannot get %d state!", state);
		return -1; }
	return 0;
}
//-------------------------------------------------------------// Rysuj prze��cznik warstw
int switch_layer(int dx, int dy, int state)
{
	// Przyjmuje jeden z maksymalnie 4 stan�w, gdzie ka�dy odpowiada za aktualn� warstw�
	if (state == 0)
		al_draw_bitmap_region(menu_buttons, 2*TILE_SIZE, 3*TILE_SIZE, TILE_SIZE, TILE_SIZE, dx*TILE_SIZE, dy*TILE_SIZE, 0);
	else if (state > 0 && state <= 4)
		al_draw_bitmap_region(menu_buttons, (state - 1)*TILE_SIZE, 2*TILE_SIZE, TILE_SIZE, TILE_SIZE, dx*TILE_SIZE, dy*TILE_SIZE, 0);
	else {
		printf("ERROR: switch_layer cannot get %d state!", state);
		return -1; }
	return 0;
}

//-------------------------------------------------------------// Rysuj SCROLL UP BUTTON
int scroll_up_button(bool state)
{
	// Przyjmuje jeden z 2 stan�w:
	// false = normalny przycisk
	// true = pod�wietlony przycisk
	if (state == false)
		al_draw_bitmap_region(menu_tile, 0,			3*TILE_SIZE, TILE_SIZE, TILE_SIZE, 38*TILE_SIZE, 0, 0);
	else if (state == true)
		al_draw_bitmap_region(menu_tile, TILE_SIZE,	3*TILE_SIZE, TILE_SIZE, TILE_SIZE, 38*TILE_SIZE, 0, 0);
	else {
		printf("ERROR: scroll_up_button cannot get %d state!", state);
		return -1; }
	return 0;
}
//-------------------------------------------------------------// Rysuj SCROLL DOWN BUTTON
int scroll_down_button(bool state)
{
	// Przyjmuje jeden z 2 stan�w:
	// false = normalny przycisk
	// true = pod�wietlony przycisk
	if (state == false)
		al_draw_bitmap_region(menu_tile, 2*TILE_SIZE, 3*TILE_SIZE, TILE_SIZE, TILE_SIZE, 38*TILE_SIZE, 29*TILE_SIZE, 0);
	else if (state == true)
		al_draw_bitmap_region(menu_tile, 3*TILE_SIZE, 3*TILE_SIZE, TILE_SIZE, TILE_SIZE, 38*TILE_SIZE, 29*TILE_SIZE, 0);
	else {
		printf("ERROR: scroll_down_button cannot get %d state!", state);
		return -1; }
	return 0;
}


//-------------------------------------------------------------// MAP RESET & GENERATE
int reset_map(int r_x, int r_y, int realloc_flag)
{
	// Dynamiczna alokacja tablicy nowej mapy
	if (realloc_flag == 0) {
		tab = (int**)malloc(r_y * sizeof *tab);
		for (int i = 0; i < r_y; ++i) {
			tab[i] = (int*)malloc(r_x * sizeof **tab); }

		g_oldr_x = r_x;
		g_oldr_y = r_y;
	} else {
		for (int i = 0; i < g_oldr_y; ++i) {
			free(tab[i]); }
		free(tab);

		tab = (int**)malloc(r_y * sizeof *tab);
		for (int i = 0; i < r_y; ++i) {
			tab[i] = (int*)malloc(r_x * sizeof **tab); }

		g_oldr_x = r_x;
		g_oldr_y = r_y;
	}

	// Wype�nianie nawej tablicy mapy warto�ciami -1
	for (int i = 0; i < r_y; ++i) {
		for (int j = 0; j < r_x; ++j) {
			tab[i][j] = -1; } }

	return 0;
}


//-------------------------------------------------------------// DIM BACKGROUND
int dim(ALLEGRO_DISPLAY *display)
{
	ALLEGRO_BITMAP *dim = al_create_bitmap(al_get_display_width(display), al_get_display_height(display));
	al_set_target_bitmap(dim);
	al_clear_to_color(al_map_rgba(0, 0, 0, 200));
	al_set_target_backbuffer(display);
	al_draw_bitmap(dim, 0, 0, 0);
	al_destroy_bitmap(dim);
	return 0;
}


//-------------------------------------------------------------// SHOW MESSAGE
int show_message(const char *text, double duration)
{
	ALLEGRO_TIMER *delay = al_create_timer(0.1);
	al_start_timer(delay);
	dim(display);
	while (al_get_timer_count(delay) < duration) {
		//al_set_target_backbuffer(display);
		al_draw_textf(fontbig, al_map_rgb( 255, 204, 0), 400, 300, ALLEGRO_ALIGN_CENTRE, "%s", text);
		al_flip_display();
	}
	al_destroy_timer(delay);
	return 0;
}


//-------------------------------------------------------------// GUI NUMERIC INPUT !
int gui_num_in(ALLEGRO_EVENT_QUEUE *event_queue, ALLEGRO_FONT *font, int number_of_digits, int dx, int dy)
{
	// ZMIENI NAZWY ZMIENNYCH NA ANG.
	al_set_physfs_file_interface();
	int i;
	ALLEGRO_BITMAP *bg = al_load_bitmap("bg.bmp");
	ALLEGRO_EVENT input;
	int *wymiar;
	wymiar = (int *)malloc(number_of_digits * sizeof *wymiar);
	int rezultat = 0;
	int wi = 0;

	//// 1
	//al_draw_bitmap_region(bg, 0, 0, 20, 60, dx, dy, 0);
	//// 2
	//for (i = 1; i <= 5; ++i) {
	//	al_draw_bitmap_region(bg, 20, 0, 20, 60, dx + (20 * i), dy, 0);
	//}
	//// 3
	//al_draw_bitmap_region(bg, 60, 0, 20, 60, dx + 120, dy, 0);
	//// 4
	//for (i = 1; i <= number_of_digits; ++i) {
	//	al_draw_bitmap_region(bg, 20, 0, 20, 60, dx + 120 + (20 * i), dy, 0);
	//}
	//// 5
	//al_draw_bitmap_region(bg, 40, 0, 20, 60, dx + 120 + (20 * i), dy, 0);

	al_draw_bitmap_region(bg, 0, 0, 20, 60, dx, dy, 0);
	for (i = 1; i <= number_of_digits; ++i) {
		al_draw_bitmap_region(bg, 20, 0, 20, 60, dx + (20 * i), dy, 0); }
	al_draw_bitmap_region(bg, 40, 0, 20, 60, dx + (20 * i), dy, 0);

	al_flip_display();

	while (1)
	{
		al_wait_for_event(event_queue, &input);
		if (input.type == ALLEGRO_EVENT_KEY_UP) {
			switch (input.keyboard.keycode) {
				case ALLEGRO_KEY_0:
					if (wi < number_of_digits)	wymiar[wi] = 0;
					break;
				case ALLEGRO_KEY_1:
					if (wi < number_of_digits)	wymiar[wi] = 1;
					break;
				case ALLEGRO_KEY_2:
					if (wi < number_of_digits)	wymiar[wi] = 2;
					break;
				case ALLEGRO_KEY_3:
					if (wi < number_of_digits)	wymiar[wi] = 3;
					break;
				case ALLEGRO_KEY_4:
					if (wi < number_of_digits)	wymiar[wi] = 4;
					break;
				case ALLEGRO_KEY_5:
					if (wi < number_of_digits)	wymiar[wi] = 5;
					break;
				case ALLEGRO_KEY_6:
					if (wi < number_of_digits)	wymiar[wi] = 6;
					break;
				case ALLEGRO_KEY_7:
					if (wi < number_of_digits)	wymiar[wi] = 7;
					break;
				case ALLEGRO_KEY_8:
					if (wi < number_of_digits)	wymiar[wi] = 8;
					break;
				case ALLEGRO_KEY_9:
					if (wi < number_of_digits)	wymiar[wi] = 9;
					break;
				case ALLEGRO_KEY_BACKSPACE:
					if (wi > 0) {
						al_draw_bitmap_region(bg, 20, 0, 20, 60, dx + (20 * wi), dy, 0);
						al_flip_display();
						--wi;
					}
					continue;
				case ALLEGRO_KEY_ENTER:
					if (wi > 0) {
						int multi = 1;
						for (int j = 1; j < wi; ++j)
							multi *= 10;
						for (int i = 1; i <= wi; ++i) {
							rezultat += wymiar[i - 1] * multi;
							multi /= 10;
						}
						free(wymiar);
						al_destroy_bitmap(bg);
						al_set_standard_file_interface();
						return rezultat;
					}
				default:
					continue;
			}
			if (wi < 3) {
				al_draw_textf(font, al_map_rgb(255, 204, 0), dx + 20 + (20 * wi), dy + 20, ALLEGRO_ALIGN_LEFT, "%3d", wymiar[wi]);
				al_flip_display();
				++wi;
			}
		}
	}
}


//-------------------------------------------------------------// NEW MAP ACTION
int new_map_action(void)
{
	dim(display);

	// Wczytanie wymiaru poziomego nowej mapy (w kafelkach)
	al_draw_text(fontbig, al_map_rgb(255, 255, 255), 220, 250, 0, "Enter new map width (max 400):");
	map_width = gui_num_in(event_queue, fontbig, 3, 460, 230);

	// Wczytanie wymiaru pionowego nowej mapy (w kafelkach)
	al_draw_text(fontbig, al_map_rgb(255, 255, 255), 220, 330, 0, "Enter new map height (max 400):");
	map_height = gui_num_in(event_queue, fontbig, 3, 460, 310);

	// Ostrze�enie o za duzych wymiarach (w oknie dialogowym)
	if ((map_width > 400) || (map_height > 400)) {
		al_show_native_message_box(display, "Wrong dimension(s)!", "Maximal map width and\\or height",
									"can\'t be greater than 400", NULL, ALLEGRO_MESSAGEBOX_WARN);
		map_width = 0;
		map_height = 0;
		return -1;
	}
	
	// Zerowanie pozycji i rysowanie bazy nowej mapy
	mpos_x = 0;
	mpos_y = 0;
	map_back(map_width, map_height);
	reset_map(map_width, map_height, 1);

	return 0;
}


//-------------------------------------------------------------// OPEN MAP ACTION !
int open_map_action(void)
{
	// WYCZY�CI� KOD W TEJ FUNKCJI!
	const char *filename = NULL;

	FILE *loadfile;

	// Otwarcie mapy do za�adowania (wida� tylko pliki o podanym rozszerzeniu)
	if (!al_show_native_file_dialog(display, file_opener)) {
		//if(file_opener) al_destroy_native_file_dialog(file_opener);
		return 1;
	}
	filename = al_get_native_file_dialog_path(file_opener, 0);
	strcpy(old_sname, filename);

	printf("ERR 2\n");

	int a;
	int k = 0;
	int result;

	// Otwieranie strumienia-pliku do odczytu
	loadfile = fopen(filename, "r");
	if (!loadfile) {
		printf("Cannot open \"%s\"\n", filename);
		return -1;
	}

	do
	{
		result = fscanf(loadfile, "%d", &a);
		if (result == 1) {
			if (k < 1) 
				map_height = a;
			++k;
		} else if (!result) {
			result = fscanf(loadfile, "%1*s");
			if (k >= 2)
				break;
		}
	}
	while (result != EOF);
	map_width = a;
	printf("Map dimensions: %d, %d", map_width, map_height);

	printf("\n\n---------------------\n\n");

	int rozmiar = map_width * map_height;
	int *vec;
	
	vec = (int*)malloc(rozmiar * sizeof *vec);
	
	int n;
	int run = 0;

	printf("Loading");
	do
	{
		result = fscanf(loadfile, "%d", &n);
		if (result == 1) {
				vec[run] = n;
				printf("%2\n", vec[run]);
			++run;
		} else if (!result) {
				result = fscanf(loadfile, "%1*s");
				printf("LOADING...\n", run);
		}
	}
	while (result != EOF);
	fclose(loadfile);

	printf("\nMap loaded!\n\n---------------------\n\n");

	mpos_x = 0;
	mpos_y = 0;
	map_back(map_width, map_height);
	reset_map(map_width, map_height, 1);

	for (int l = 0; l < map_height; ++l) {
		for (int j = 0; j < map_width; ++j) {
			tab[l][j] = vec[j+(l*map_width)];
		}
	}

	// Rysowanie wczytanej mapy w oknie programu
	//
	int op_w = al_get_bitmap_width(palette);
	op_w = op_w/TILE_SIZE;
	al_set_target_bitmap(buffer);
	for (int x = 0; x < map_width; ++x) {
		for (int y = 0; y < map_height; ++y) {
			al_draw_bitmap_region(palette, (tab[y][x] % op_w)*TILE_SIZE, (tab[y][x] / op_w)*TILE_SIZE, TILE_SIZE, TILE_SIZE, x*TILE_SIZE, y*TILE_SIZE, 0);
		}
	}

	free(vec);
	printf("Map opened!\n");

	return 0;
}


//-------------------------------------------------------------// SAVE MAP ACTION
int save_map_action(bool cf_state)
{
	FILE *savefile;
	char ext[4] = "txt";

	// Zapisanie nowej mapy po now� nazw�
	if (cf_state) {
		const char *filename = NULL;
		if (!al_show_native_file_dialog(display, file_saver)) {
			if(file_saver) al_destroy_native_file_dialog(file_saver);
			return 1;
		}
		filename = al_get_native_file_dialog_path(file_saver, 0);
		savefile = fopen(filename, "w");
		if (!savefile) {
			printf("ERROR: Cannot create file %s\n", filename);
			return -1;
		}
		strcpy(old_sname, filename);
	// Nadpisanie otwartej mapy pod star� nazw�
	} else if (!cf_state) {
		const char *filename = old_sname;
		savefile = fopen(filename, "w");
		if (!savefile) {
			printf("ERROR: Cannot create file %s\n", filename);
			return -1;
		}
	}

	fprintf(savefile, "[%d][%d]=\n{\n", map_height, map_width);

	// Zapisanie mapy i jej wymiar�w jako macierzy (tablicy) w pliku
	for (int i = 0; i < map_height; ++i) {
		for (int j = 0; j < map_width; ++j) {
			fprintf(savefile, "%d,", tab[i][j]); }
		fprintf(savefile, "\n"); }
	fprintf(savefile, "};");
	
	fclose(savefile);
	show_message("Map saved!", 7.0);
	printf("Map saved!\n");

	

	return 0;
}


//-------------------------------------------------------------// Rysuj PALETTE
int draw_palette(int move)
{
	printf("scroll_value = %d\n", move);
	
	if (palette_buffer)
		al_destroy_bitmap(palette_buffer);
	int b_w = al_get_bitmap_width(palette);			// b_w = szeroko�� bitmapy palety w pikselach
	int b_h = al_get_bitmap_height(palette);		// b_h = wysoko�� bitmapy palety w pikselach
	int b_tw = b_w / TILE_SIZE;						// b_tw = szeroko�� bitmapy palety w kafelkach
	int b_th = b_h / TILE_SIZE;						// b_th = wysoko�� bitmapy palety w kafelkach
	int p_y = (b_w*b_h) / (TILE_SIZE * TILE_SIZE);	// Ilo�� wszystkich kafelk�w w palecie

	if (p_y < 28)
		palette_buffer = al_create_bitmap(TILE_SIZE, p_y * TILE_SIZE);
	else
		palette_buffer = al_create_bitmap(TILE_SIZE, 28 * TILE_SIZE);
	al_set_target_bitmap(palette_buffer);
	al_clear_to_color(al_map_rgb(192, 192, 192));

	int position = 0;				// Nie modyfikowa�! Zmienna pomocnicza.
	int x_starter = move % b_tw;
	int y_starter = move / b_tw;
	for (int y = y_starter; y < b_th ; ++y) {
		for (int x = x_starter; x < b_tw ; ++x) {
			al_draw_bitmap_region(palette, x * TILE_SIZE, y * TILE_SIZE, TILE_SIZE, TILE_SIZE, 0, position * TILE_SIZE, 0);
			++position;
		}
		x_starter = 0;
	}
	
	/*
	al_set_target_bitmap(buffer);
	for (int x = 0; x < map_width; ++x) {
		for (int y = 0; y < map_height; ++y) {
			al_draw_bitmap_region(palette, (tab[y][x] % b_tw)*TILE_SIZE, (tab[y][x] / b_tw)*TILE_SIZE, TILE_SIZE, TILE_SIZE, x*TILE_SIZE, y*TILE_SIZE, 0);
		}
	}
	*/

	return p_y;
}


//-------------------------------------------------------------// Rysowanie prostok�t�w
int draw_rect(int x1, int y1, int x2, int y2, int tile)
{
	printf("Drawing rectangle\n");

	// Sprawdzenie czy x1,y1 < x2,y2 i ewentualna zamiana.
	int r_buffer;
	if (x1 > x2) {
		r_buffer = x1;
		x1 = x2;
		x2 = r_buffer; }
	if (y1 > y2) {
		r_buffer = y1;
		y1 = y2;
		y2 = r_buffer; }

	//Przesyni�cie prostok�ta o warto�� przesuni�cia ca�ej mapy (w sensie kursorami)
	if (mpos_x != 0) {
		x1 -= (mpos_x/TILE_SIZE);
		x2 -= (mpos_x/TILE_SIZE); }
	if (mpos_y != 0) {
		y1 -= (mpos_y/TILE_SIZE);
		y2 -= (mpos_y/TILE_SIZE); }

	// Wpisywanie warto�ci kafelka, kt�rym rysowany jest
	// prostok�t, do tablicy mapy. Oczywi�cie tylko ten
	// fragment, gdzie jest prostok�t.
	for (int i = y1; i <= y2; ++i) {
		for (int j = x1; j <= x2; ++j) {
			tab[i][j] = tile;
		}
	}
	return 0;
}
//-------------------------------------------------------------// Rysowanie prostok�t�w
int draw_line(int x1, int y1, int x2, int y2, int tile)
{
	printf("Drawing line\n");

	//Przesyni�cie linii o warto�� przesuni�cia ca�ej mapy (w sensie kursorami na klawiaturze)
	if (mpos_x != 0) {
		x1 -= (mpos_x/TILE_SIZE);
		x2 -= (mpos_x/TILE_SIZE); }
	if (mpos_y != 0) {
		y1 -= (mpos_y/TILE_SIZE);
		y2 -= (mpos_y/TILE_SIZE); }

	// Linia
	int dx, dy;
	int kx, ky;
	int e;

	// Poni�szy kod to pe�ny algorytm Bresenhama.
	// Kod jest napisany na podstawie tego, co mo�na
	// znale�� pod poni�szym adresem:
	// http://edu.i-lo.tarnow.pl/inf/utils/002_roz/2008_06.php
	//
	if (x1 <= x2) kx = 1;
	else kx = -1;
	printf("kx: %i\n", kx);
	
	if (y1 <= y2) ky = 1;
	else ky = -1;
	printf("ky: %i\n", ky);

	if ((dx = x2 - x1) < 0)
		dx = -dx;
	printf("dx: %i\n", dx);

	if ((dy = y2 - y1) < 0)
		dy = -dy;
	printf("dy: %i\n", dy);

	tab[y1][x1] = tile;

	if (dx >= dy) {
		e = dx / 2;
		for (int i = 1; i <= dx; ++i) {
			x1 += kx;
			e -=dy;
			if (e < 0) {
				y1 += ky;
				e +=dx;
			}
			tab[y1][x1] = tile;
		}
	} else if (dx < dy) {
		e = dy / 2;
		for (int i = 1; i <= dy; ++i) {
			y1 += ky;
			e -= dx;
			if (e < 0) {
				x1 += kx;
				e += dy;
			}
			tab[y1][x1] = tile;
		}
	}
	return 0;
}
//-------------------------------------------------------------// Przerysowanie samej mapy
int redraw_map(void)
{
	int op_w = (al_get_bitmap_width(palette))/TILE_SIZE;
	int mxt = (mpos_x/TILE_SIZE);
	int myt = (mpos_y/TILE_SIZE);
	int mw, mh;

	if (map_width < DW) mw = map_width; else mw = DW;
	if (map_height < DH) mh = map_height; else mh = DH;

	for (int x = 0 - mxt; x < mw - mxt; ++x) {
		for (int y = 0 - myt; y < mh - myt; ++y) {
			if (tab[y][x] >= 0) {
				al_set_target_bitmap(buffer);
				al_draw_bitmap_region(palette, (tab[y][x] % op_w)*TILE_SIZE, (tab[y][x] / op_w)*TILE_SIZE, TILE_SIZE, TILE_SIZE, (x + mxt )*TILE_SIZE, (y + myt)*TILE_SIZE, 0);
			}
		}
	}
	return 0;
}
//-------------------------------------------------------------// Zmiana rozmiaru mapy
int resize_map(void)
{
	dim(display);

	// Wczytanie nowego wymiaru poziomego mapy (w kafelkach)
	al_draw_text(fontbig, al_map_rgb(255, 255, 255), 220, 250, 0, "Enter new map width (max 400):");
	int new_width = gui_num_in(event_queue, fontbig, 3, 460, 230);

	// Wczytanie nowego wymiaru pionowego mapy (w kafelkach)
	al_draw_text(fontbig, al_map_rgb(255, 255, 255), 220, 330, 0, "Enter new map height (max 400):");
	int new_height = gui_num_in(event_queue, fontbig, 3, 460, 310);

	// Ostrze�enie o za du�ych wymiarach (w oknie dialogowym)
	if ((map_width > 400) || (map_height > 400)) {
		al_show_native_message_box(display, "Wrong dimension(s)!", "Maximal map width and\\or height",
									"can\'t be greater than 400", NULL, ALLEGRO_MESSAGEBOX_WARN);
		return -1;
	}

	tab = (int**)realloc(tab, new_height * sizeof *tab);
	for (int i = map_height; i < new_height; ++i)
		tab[i] = NULL;

	for (int i = 0; i < new_height; ++i) {
		tab[i] = (int*)realloc(tab[i], new_width * sizeof *tab);
	}

	// Wpisywanie '-1' do mapy rozszerzonej w poziomie
	if (new_width > map_width && new_height <= map_height) {
		for (int i = map_width; i < new_width; ++i) {
			for (int j = 0; j < new_height; ++j) {
				tab[j][i] = -1;
			}
		}
	// Wpisywanie '-1' do mapy rozszerzonej w pionie
	} else if (new_width <= map_width && new_height > map_height) {
		for (int i = map_height; i < new_height; ++i) {
			for (int j = 0; j < new_width; ++j) {
				tab[i][j] = -1;
			}
		}
	// Wpisywanie '-1' do mapy rozszerzonej w obu kierunkach
	// Najpierw wpisywanie na dole, a potem w pozosta�ym obszarze
	// w prawym g�rnym rogu tablicy.
	} else if (new_width > map_width && new_height > map_height) {
		for (int i = map_height; i < new_height; ++i) {
			for (int j = 0; j < new_width; ++j) {
				tab[i][j] = -1;
			}
		}
		for (int i = map_width; i < new_width; ++i) {
			for (int j = 0; j < map_height; ++j) {
				tab[j][i] = -1;
			}
		}
	}

	//tab = (int**)malloc(map_height * sizeof *tab);
	//for (int i = 0; i < map_height; ++i) {
	//	tab[i] = (int*)malloc(map_width * sizeof **tab); }

	map_width = new_width;
	map_height = new_height;

	g_oldr_x = map_width;
	g_oldr_y = map_height;
	map_back(map_width, map_height);
	return 0;
}


//-------------------------------------------------------------// REDRAWAL
int redrawal(int rcur_x, int rcur_y, int picker_info, bool picked_tile_flag, int scr_val, bool is_palette_drawn, int palette_size, bool paint, bool drf, int rx1, int ry1, bool dlf)
{
	al_set_target_backbuffer(display);
	al_clear_to_color(al_map_rgb(0, 0, 0));
	al_draw_bitmap(buffer, 0, 0, 0);							// Przerysuj t�o/map�
	al_draw_bitmap(palette_menu_buffer, DW*TILE_SIZE, 0, 0);	// Przerysuj menu palety
	al_draw_bitmap(menu_buffer, 0, DH*TILE_SIZE, 0);			// Przerysuj menu g��wne

	// (Prze)rysuj palet�
	if (palette != NULL) {
		al_draw_bitmap(palette_buffer, (DW + 1) * TILE_SIZE, TILE_SIZE, 0);	}
	
	// Przerysuj przycisk 'new'
	if ((rcur_x > TILE_SIZE) && (rcur_x < 2*TILE_SIZE) && (rcur_y > 28*TILE_SIZE) && (rcur_y < 29*TILE_SIZE))
		draw_button(2, 0, 1, 28, true); else draw_button(2, 0, 1, 28, false);
	// Przerysuj przycisk 'open'
	if ((rcur_x > 3*TILE_SIZE) && (rcur_x < 4*TILE_SIZE) && (rcur_y > 28*TILE_SIZE) && (rcur_y < 29*TILE_SIZE))
		draw_button(2, 1, 3, 28, true); else draw_button(2, 1, 3, 28, false);
	// Przerysuj przycisk 'save'
	if ((rcur_x > 5*TILE_SIZE) && (rcur_x < 6*TILE_SIZE) && (rcur_y > 28*TILE_SIZE) && (rcur_y < 29*TILE_SIZE))
		draw_button(0, 0, 5, 28, true); else draw_button(0, 0, 5, 28, false);
	// Przerysuj przycisk 'normal'
	if ((rcur_x > 13*TILE_SIZE) && (rcur_x < 14*TILE_SIZE) && (rcur_y > 28*TILE_SIZE) && (rcur_y < 29*TILE_SIZE))
		draw_button(0, 6, 13, 28, true); else draw_button(0, 6, 13, 28, false);
	// Przerysuj przycisk 'resize'
	if ((rcur_x > 15*TILE_SIZE) && (rcur_x < 16*TILE_SIZE) && (rcur_y > 28*TILE_SIZE) && (rcur_y < 29*TILE_SIZE))
		draw_button(0, 4, 15, 28, true); else draw_button(0, 4, 15, 28, false);
	// Przerysuj przycisk 'line'
	if ((rcur_x > 17*TILE_SIZE) && (rcur_x < 18*TILE_SIZE) && (rcur_y > 28*TILE_SIZE) && (rcur_y < 29*TILE_SIZE))
		draw_button(2, 4, 17, 28, true); else draw_button(2, 4, 17, 28, false);
	// Przerysuj przycisk 'rectangle'
	if ((rcur_x > 19*TILE_SIZE) && (rcur_x < 20*TILE_SIZE) && (rcur_y > 28*TILE_SIZE) && (rcur_y < 29*TILE_SIZE))
		draw_button(0, 5, 19, 28, true); else draw_button(0, 5, 19, 28, false);
	// Przerysuj przycisk 'floodfill'
	if ((rcur_x > 21*TILE_SIZE) && (rcur_x < 22*TILE_SIZE) && (rcur_y > 28*TILE_SIZE) && (rcur_y < 29*TILE_SIZE))
		draw_button(2, 5, 21, 28, true); else draw_button(2, 5, 21, 28, false);
	// Przerysuj przycisk 'info'
	if ((rcur_x > 34*TILE_SIZE) && (rcur_x < 35*TILE_SIZE) && (rcur_y > 28*TILE_SIZE) && (rcur_y < 29*TILE_SIZE))
		draw_button(0, 3, 34, 28, true); else draw_button(0, 3, 34, 28, false);
	// Przerysuj przycisk 'load palette'
	if ((rcur_x > 36*TILE_SIZE) && (rcur_x < 37*TILE_SIZE) && (rcur_y > 28*TILE_SIZE) && (rcur_y < 29*TILE_SIZE))
		draw_button(0, 1, 36, 28, true); else draw_button(0, 1, 36, 28, false);

	// Przerysuj przycisk 'layers'
	if ((rcur_x > 27*TILE_SIZE) && (rcur_x < 28*TILE_SIZE) && (rcur_y > 28*TILE_SIZE) && (rcur_y < 29*TILE_SIZE))
		switch_layer(27, 28, 0); else switch_layer(27, 28, layer);

	// Rysowanie wska�nik�w narz�dzi
	if (drf) {
		al_draw_bitmap_region(menu_tile, 3*TILE_SIZE, 0, TILE_SIZE, TILE_SIZE, 19*TILE_SIZE, 27*TILE_SIZE, 0);
		al_draw_bitmap_region(menu_tile, 3*TILE_SIZE, TILE_SIZE, TILE_SIZE, TILE_SIZE, 19*TILE_SIZE, 29*TILE_SIZE, 0);
	} else if (dlf){
		al_draw_bitmap_region(menu_tile, 3*TILE_SIZE, 0, TILE_SIZE, TILE_SIZE, 17*TILE_SIZE, 27*TILE_SIZE, 0);
		al_draw_bitmap_region(menu_tile, 3*TILE_SIZE, TILE_SIZE, TILE_SIZE, TILE_SIZE, 17*TILE_SIZE, 29*TILE_SIZE, 0);
	} else {
		al_draw_bitmap_region(menu_tile, 3*TILE_SIZE, 0, TILE_SIZE, TILE_SIZE, 13*TILE_SIZE, 27*TILE_SIZE, 0);
		al_draw_bitmap_region(menu_tile, 3*TILE_SIZE, TILE_SIZE, TILE_SIZE, TILE_SIZE, 13*TILE_SIZE, 29*TILE_SIZE, 0);
	}

	// Przerysowuje kursor kafelkowy i sprawia,
	// �e kursor "przeskakuje" po polach kafelk�w.
	// To dotyczy TYLKO kursora, a nie "malowania"
	// po mapie.
	if ((rcur_x < DW*TILE_SIZE) && (rcur_y < DH*TILE_SIZE) && !paint) {
		al_draw_bitmap(current_tile, (rcur_x/TILE_SIZE)*TILE_SIZE, (rcur_y/TILE_SIZE)*TILE_SIZE, 0);
		if (picked_tile_flag == true) {
			al_convert_mask_to_alpha(menu_buttons, al_map_rgb(255, 0, 255));
			al_draw_bitmap_region(menu_buttons, 3*TILE_SIZE, 3*TILE_SIZE, TILE_SIZE, TILE_SIZE,
				(rcur_x/TILE_SIZE)*TILE_SIZE, (rcur_y/TILE_SIZE)*TILE_SIZE, 0);
		}
	}

	// Wy�wietlanie numeracji p�l
	for (int vx = 0; vx < 37; ++vx) {
		al_draw_textf(font, al_map_rgb(255, 255, 255), (vx*TILE_SIZE)+10, 0, ALLEGRO_ALIGN_CENTRE, "%d", (vx+1)-(mpos_x/(TILE_SIZE))); }
	for (int vy = 0; vy < 27; ++vy) {
		al_draw_textf(font, al_map_rgb(255, 255, 255), 0, (vy*TILE_SIZE)+10, ALLEGRO_ALIGN_LEFT, "%d", (vy+1)-(mpos_y/(TILE_SIZE))); }

	// Wy�wietla pozycj� kursora przy nim
	if (!paint)
		al_draw_textf(font, al_map_rgb(255, 255, 255), rcur_x+10, rcur_y+TILE_SIZE, ALLEGRO_ALIGN_LEFT, "%3d - %3d", rcur_x, rcur_y);

	// Numer aktualnie wybranego z palety kafelka
	al_draw_textf(fontbig, al_map_rgb(255, 255, 255), (7*TILE_SIZE)+2, (28*TILE_SIZE)-2, 0, "Picked tile: %d", picker_info);

	// Numer aktualnie wybranej warstwy mapy
	al_draw_textf(fontbig, al_map_rgb(255, 255, 255), (23*TILE_SIZE)+2, (28*TILE_SIZE)-2, 0, "Layer: %d", layer);

	//Z TYM TRZEBA ZROBI� TAK JAK Z RESZT� PRZYCISK�W!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	// Oba poni�sze przyciski rysuj� si� w zale�no�ci
	// od okre�lonych warunk�w spe�nianych przez palet�.
	// Przerysuj przycisk 'scroll down'.
	if ((is_palette_drawn == true) && ((palette_size - scr_val) > 28)) {
		if ((rcur_x > 38*TILE_SIZE) && (rcur_x < 39*TILE_SIZE) && (rcur_y > 29*TILE_SIZE))
			scroll_down_button(1);
		else
			scroll_down_button(0);
	}
	// Przerysuj przycisk 'scroll up'.
	if ((is_palette_drawn == true) && (scr_val > 0)) {
		if ((rcur_x > 38*TILE_SIZE) && (rcur_x < 39*TILE_SIZE) && (rcur_y < TILE_SIZE))
			scroll_up_button(1);
		else
			scroll_up_button(0);
	}

	// Jesli nie wybrano kafelka z palety, to kursor
	// jest niebieski na mapie i bia�y poza ni�.
	if (picked_tile_flag == false) {
		al_set_target_bitmap(current_tile);
		if ((rcur_x > (map_width*TILE_SIZE)+mpos_x) ||
			(rcur_y > (map_height*TILE_SIZE)+mpos_y) ||
			(rcur_x > (DW*TILE_SIZE)) ||
			(rcur_y > (DH*TILE_SIZE))) {
			al_clear_to_color(al_map_rgba(127, 127, 127, 127)); }
		else {
			al_clear_to_color(al_map_rgba(64, 64, 255, 127)); }
	}

	// Rysowanie efekt�w  uzycia narz�dzi rysowania
	if ((rcur_x < (DW*TILE_SIZE)) && (rcur_y < (DH*TILE_SIZE)) &&
		(rcur_x < (map_width*TILE_SIZE)) && (rcur_y < (map_height*TILE_SIZE))) {
		// Pod�wietlanie obszaru podczas rysowania linii
			if (dlf && picked_tile_flag && rx1 >= 0 && ry1 >= 0) {
				al_set_target_backbuffer(display);
				// Ca�y algorytm rysowania linii jest tu powt�rzony
				int rx2 = rcur_x/TILE_SIZE;
				int ry2 = rcur_y/TILE_SIZE;
				int drx, dry;
				int krx, kry;
				int e;

				if (rx1 <= rx2) krx = 1; else krx = -1;
				if (ry1 <= ry2) kry = 1; else kry = -1;
				if ((drx = rx2 - rx1) < 0) drx = -drx;
				if ((dry = ry2 - ry1) < 0) dry = -dry;
				al_draw_bitmap(current_tile, rx1 * TILE_SIZE, ry1 * TILE_SIZE, 0);

				if (drx >= dry) {
					e = drx / 2;
					for (int i = 1; i <= drx; ++i) {
						rx1 += krx;
						e -=dry;
						if (e < 0) {
							ry1 += kry;
							e +=drx; }
						al_draw_bitmap(current_tile, rx1 * TILE_SIZE, ry1 * TILE_SIZE, 0); }
				} else if (drx < dry) {
					e = dry / 2;
					for (int i = 1; i <= dry; ++i) {
						ry1 += kry;
						e -= drx;
						if (e < 0) {
							rx1 += krx;
							e += dry; }
						al_draw_bitmap(current_tile, rx1 * TILE_SIZE, ry1 * TILE_SIZE, 0); }
				}
		// Pod�wietlanie obszaru podczas rysowania prostok�ta
			} else if (drf && picked_tile_flag && rx1 >= 0 && ry1 >= 0) {
				al_set_target_bitmap(w_square);
				al_clear_to_color(al_map_rgba(64, 64, 64, 10));
				al_set_target_backbuffer(display);
				// Wprowadzone tu s� 3 zmienne pomocnicze o ma�ym zakresie
				int rx2 = rcur_x/TILE_SIZE;
				int ry2 = rcur_y/TILE_SIZE;
				int rr_buffer = 0;
				if (ry1 > ry2) {
					rr_buffer = ry1;
					ry1 = ry2;
					ry2 = rr_buffer; }
				if (rx1 > rx2) {
					rr_buffer = rx1;
					rx1 = rx2;
					rx2 = rr_buffer; }

				for (int i = ry1; i <= ry2; ++i) {
					for (int j = rx1; j <= rx2; ++j) {
						al_draw_bitmap(w_square, j * TILE_SIZE, i * TILE_SIZE, 0);
					}
				}
		// Malowanie zwyk�e
			} else if (paint && (rcur_x > 0) && (rcur_y > 0)){
				tab[(rcur_y-mpos_y)/TILE_SIZE][(rcur_x-mpos_x)/TILE_SIZE] = picker_info;
			}
	}
	map_back(map_width, map_height);
	if (is_palette_drawn) redraw_map();

	al_flip_display();

	return 0;
}


//-------------------------------------------------------------// MAIN
int main(void)
{
	bool changes_flag = false;			// Flaga zmian wprowadzonych na edytowanej mapie (true = s� zmiany, false = nie ma)
	bool name_change_flag = true;		// Flaga zmiany nazwy edytowanej mapy (true = potrzebna nowa nazwa, false = nadpisz pod star�)
	bool picker_flag = false;			// Flaga podniesienia (true = podniesiony jest jakikolwiek kafelek, false = nic nie jest podniesione)
	bool palette_drawn_flag = false;	// Flaga narysowanej palety (true = paleta zaladowana i narysowana, false = brak palety)
	bool painting = false;				// Flaga "malowania" ci�g�ego (true = maluj�, false = brak akcji)

	int scroll_value = 0;				// Warto�� przesuni�cie (przewini�cia) palety w kafelkach
	int picker_mov = 0;					// Poprawianie warto�ci pickera po przewini�ciu palety (o ile kafelk�w)
	int palette_tiles_number = 0;		// Szeroko��*wysoko�� za�adowanej palety (czyli ilo�� kafelk�w)
	int palette_s_size = 0;				// Rozmiar palety o kafelkach != 20
	int pss_result = 0;					// Rezultat wczytania palety
	int p_w = 0, p_h = 0;				// Wymiary 'palette', czyli palety wewn�trznej programu, przeskalowanej (w px)
	int ps_w = 0, ps_h = 0;				// Wymiary 'palette_s' czyli palety �adowanej (w px)
	double scale_coef = 1;				// Wsp�czynnik skali dla powy�szych
	int p_w_t= 0;						// Szeroko�� 'palette' w kafelkach (uzywane do podnoszenia kafelk�w z mapy)

	bool draw_rectangle_flag = false;	// Flaga narz�dzia rysowania prostok�ta
	int rectx1 = -20;					// Warto�ci rectx1 i recty1 musz� pocz�tkowo wynosi� -20, gdy� p�niej
	int recty1 = -20;					// w funkcji redrawal, jest warunek �e pod�wietlenie miejsca prostok�ta
	int rectx2 = 0;						// musi zaczyna� si� od miejsca >= 0. Same zmienne oznaczaj� pierwszy
	int recty2 = 0;						// i drugi z wskazywanych mysz� wierzcho�k�w prostok�ta.

	bool draw_line_flag = false;		// Flaga narz�dzia rysowania linii. Reszta zmiennych u�yta jak w prostok�cie.

	PHYSFS_init(NULL);					// W��czenie PhysFS potrzebnego do obs�ugi archiw�w (np. plik�w .dat)
	PHYSFS_addToSearchPath(".", 1);
	PHYSFS_addToSearchPath("data.dat", 1);
	al_set_physfs_file_interface();
	

	ALLEGRO_EVENT ev;					// Allegro event
	init();								// Funkcja inicjalizacyjna

	int map_check = 1;					// Tworzenie nowej mapy, nie wi�kszej ni� 400x400
	do
	{
		al_set_target_backbuffer(display);
		al_clear_to_color(al_map_rgb(0, 0, 0));
		map_check = new_map_action();
	}
	while (map_check != 0);

	int cur_x = NULL;					// Pozycja kursora w warto�ciach ca�kowitych INTEGER
	int cur_y = NULL;
	int redraw = 1;						// Flaga przerysowania zawarto�ci okna
	int picker = 0;						// Numer (ID) aktualnie trzymanego kafelka

	draw_palette_menu();				// Rysuj menu palety
	draw_menu();						// Rysuj menu


// ----------------------------------------------------------------------------- G��WNA P�TLA ZACZYNA SI� TUTAJ!!!
	while(1) {
		al_wait_for_event(event_queue, &ev);

		if (ev.type == ALLEGRO_EVENT_TIMER) {
			redraw = 1;
		} else if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
// Wyj�cie z programu przez zamkni�cie okna
			if (al_show_native_message_box(display, "Exit", "Are you sure you want to exit?",
				"Make sure you saved your map!", NULL, ALLEGRO_MESSAGEBOX_YES_NO)-1)
				printf("NO EXIT\n");
			else
			return 0;
		} else if (ev.type == ALLEGRO_EVENT_MOUSE_AXES ||
				   ev.type == ALLEGRO_EVENT_MOUSE_ENTER_DISPLAY) {
					   cur_x = ev.mouse.x;	// Pobierz pozycj� x kursora
					   cur_y = ev.mouse.y;	// Pobierz pozycj� y kursora
// Przewijanie palety za pomoc� rolki myszy :)
					   if (ev.mouse.z > 0) {
						   if ((palette_drawn_flag == true) && (scroll_value > 0)) {
							   --scroll_value;				// W g�r�
							   picker_mov = scroll_value;
							   draw_palette(scroll_value);
							   printf("Mouse scroll up\n\n"); }
						   al_set_mouse_z(0);
					   } else if (ev.mouse.z < 0) {
						   if ((palette_drawn_flag == true) && ((palette_tiles_number - scroll_value) > 28)) {
							   ++scroll_value;				// I w d�
							   picker_mov = scroll_value;
							   draw_palette(scroll_value);
							   printf("Mouse scroll down\n\n"); }
						   al_set_mouse_z(0);
					   }
// --------------------------------------------- Mouse input --------------------------------------------- //
		} else if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN) {
			switch (ev.mouse.button) {
			case 1:
				printf("LMB\n");
// "Malowanie"
				if ((cur_x < (DW*TILE_SIZE)) && (cur_y < (DH*TILE_SIZE)) &&
					(cur_x < (map_width*TILE_SIZE)) && (cur_y < (map_height*TILE_SIZE)) &&
					(picker_flag == true)) {
						if (!draw_rectangle_flag && !draw_line_flag)			// Je�li nie w��czono rysowania prostok�t�w to w��czone jest malowanie
							painting = true;
						else if (draw_rectangle_flag) {		// Rysowanie prostok�ta. Przy mouse button down pobierane s� warto�ci				
							painting = false;				// pierwszego wierzcho�ka.
							printf("%i\n", picker);
							rectx1 = cur_x/TILE_SIZE;
							recty1 = cur_y/TILE_SIZE;
							printf("x1: %3i, y1: %3i\n", rectx1, recty1);
						} else if(draw_line_flag) {			
							painting = false;
							printf("%i\n", picker);
							rectx1 = cur_x/TILE_SIZE;
							recty1 = cur_y/TILE_SIZE;
							printf("x1: %3i, y1: %3i\n", rectx1, recty1);
						}
						changes_flag = true;
// Podnoszenie kafelka z DU�EJ palety
				} else if ((cur_x > 38*TILE_SIZE) && (cur_x < 39*TILE_SIZE) &&
						   (cur_y > TILE_SIZE) && (cur_y < 29*TILE_SIZE) && (palette_tiles_number >= 28)) {
							   picker = ((cur_y - TILE_SIZE)/TILE_SIZE) + picker_mov;
							   printf("Picked tile number value: %2d (big palette)\n", picker);
							   al_set_target_bitmap(current_tile);
							   al_draw_bitmap_region(palette_buffer, 0, (picker-picker_mov)*TILE_SIZE, TILE_SIZE, TILE_SIZE, 0, 0, 0);
							   picker_flag = true;
// Podnoszenie kafelka z MA�EJ palety
				} else if ((cur_x > 38*TILE_SIZE) && (cur_x < 39*TILE_SIZE) &&
						   (cur_y > TILE_SIZE) && (palette_tiles_number < 28)) {
							   if  (cur_y < ((palette_tiles_number * TILE_SIZE) + TILE_SIZE)) {		// Tu jest taki myk, �e dla palette_tiles_number=0 cur_y musi byc
								   picker = ((cur_y - TILE_SIZE)/TILE_SIZE) + picker_mov;			// jednoczesnie < i > od TILE_SIZE, czyli nie do spe�nienia.
								   printf("Picked tile number value: %2d (big palette)\n", picker);
								   al_set_target_bitmap(current_tile);
								   al_draw_bitmap_region(palette_buffer, 0, (picker-picker_mov)*TILE_SIZE, TILE_SIZE, TILE_SIZE, 0, 0, 0);
								   picker_flag = true;
							   }
// Map save - zapisz map�
				} else if ((cur_x > 5*TILE_SIZE) && (cur_x < 6*TILE_SIZE) &&
						   (cur_y > 28*TILE_SIZE) && (cur_y < 29*TILE_SIZE)) {
							   save_map_action(name_change_flag);
							   name_change_flag = false;
							   changes_flag = false;
// Reset map - resetuj map�
				} else if ((cur_x > TILE_SIZE) && (cur_x < 2*TILE_SIZE) &&
						   (cur_y > 28*TILE_SIZE) && (cur_y < 29*TILE_SIZE)) {
							   if (al_show_native_message_box(display, "Create new map", "Message",
								   "Are you sure you want to create new map?", NULL, ALLEGRO_MESSAGEBOX_YES_NO)-1) {
									   printf("NO chosen\n");
							   } else if (changes_flag == false) {
								   new_map_action();
								   name_change_flag = true;
							   } else {
								   printf("YES chosen, changes.\n");
								   if (al_show_native_message_box(display, "Save current map?", "Message",
									   "Do you want to save your map?", NULL, ALLEGRO_MESSAGEBOX_YES_NO)-1) {
										   printf("NO, DON'T SAVE chosen\n");
										   new_map_action();
										   name_change_flag = true;
										   changes_flag = true;
								   } else {
									   printf("YES, SAVE chosen\n");
									   save_map_action(name_change_flag);
									   new_map_action();
									   name_change_flag = true;
									   changes_flag = false;
								   }
							   } 
// Load palette - �aduj palet�
				} else if ((cur_x > 36*TILE_SIZE) && (cur_x < 37*TILE_SIZE) && (cur_y > 28*TILE_SIZE) && (cur_y < 29*TILE_SIZE)) {
						// Wska�nik na tablic� const char do przechowywania nazwy �adowanej
						// palety. Deklaracja jest tutaj, bo zmienna musi mie� okres �ycia
						// tylko na czas �adowania palety.
					const char *palette_name;
						// Otwarcie mapy do za�adowania (wida� tylko pliki o podanym w 'init' rozszerzeniu)
					if (!al_show_native_file_dialog(display, palette_opener)) {
						al_set_physfs_file_interface();
						continue;
					}
						// Prze��czenie na standardowy interfejs plik�w (z PhysFS)
					al_set_standard_file_interface();						
						// Wczytanie pliku bitmapy palety
					palette_name = al_get_native_file_dialog_path(palette_opener, 0);
						// Podanie rozmiaru kafelka na wczytywanej palecie
					dim(display);
					al_draw_text(fontbig, al_map_rgb(255, 255, 255), 240, 290, 0, "Enter single tile size:");
					al_flip_display();
					if (palette_s_size = gui_num_in(event_queue, fontbig, 3, 410, 270))
						pss_result = 1;
					else
						pss_result = 0;
						// Sprawdzenie poprawno�ci danych i p�niej wczytanie do 'palette_s',
						// zeskalowanie i przerysowanie do 'palette' palety.
						// Mo�na w przysz�o�ci spr�bowa� prze�o�y� to na funkcj�.
					if (pss_result == 0) {
						fprintf(stderr, "ERROR: Tile size must be an integer format number!\n", 1); }
					else if (pss_result == 1) {
						palette_s = al_load_bitmap(palette_name);
						if (!palette_s) {
							fprintf(stderr, "ERROR: File \"%s\" does not exist!\n", palette_name, 1);
							al_set_physfs_file_interface();
							return 1;
						}
						scale_coef = (palette_s_size / 20.0);
						printf("Scale coefficient: %f\n", scale_coef);
						ps_w = al_get_bitmap_width(palette_s);
						printf("W: %d\n", ps_w);
						ps_h = al_get_bitmap_height(palette_s);
						printf("H: %d\n", ps_h);

							// Sprawdzenie czy podany rozmiar kafelka nie jest wiekszy od rozmiar�w bitmapy
						if ((palette_s_size > ps_w) || (palette_s_size > ps_h)) {
							dim(display);
							show_message("Tile size cannot be greater than any of the palette bitmap dimensions!", 20.0);
							printf("ERROR: Tile size cannot be greater than any of the palette bitmap dimensions!\n");
							al_set_physfs_file_interface();
							continue;
						}

						p_w = (ps_w / scale_coef);
						p_h = (ps_h / scale_coef);
							// Stworzenie 'palette' o odpowiednich wymiarach i przerysowanie do niej palety
						palette = al_create_bitmap(p_w, p_h);
						al_set_target_bitmap(palette);
						al_draw_scaled_bitmap(palette_s, 0, 0, ps_w, ps_h, 0, 0, p_w, p_h, 0);
					}
					if (!palette) {
						fprintf(stderr, "ERROR: Cannot create palette bitmap!\n", 1);
					} else {
						palette_tiles_number = draw_palette(0);
						printf("palette_tiles_number = %d\n", palette_tiles_number);
						scroll_value = 0;
						printf("Palette loaded...\n");
						palette_drawn_flag = true;
					}
					// Zerowanie warto�ci pickera i przesuni�cia
					picker = 0;
					picker_mov = 0;

					al_set_target_bitmap(current_tile);
					al_draw_bitmap_region(palette_buffer, 0, (picker-picker_mov)*TILE_SIZE, TILE_SIZE, TILE_SIZE, 0, 0, 0);
						// Obliczanie warto�ci p_w_t po za�adowaniu i narysowaniu palety
					p_w_t = p_w / TILE_SIZE;
						// Prze��czanie z powrotem na interfejs PhysFS
					al_set_physfs_file_interface();
// Open map file - Otw�rz map�
				} else if ((cur_x > 3*TILE_SIZE) && (cur_x < 4*TILE_SIZE) &&
						   (cur_y > 28*TILE_SIZE) && (cur_y < 29*TILE_SIZE)) {
							   if (palette_drawn_flag == true) {
								   open_map_action();
								   name_change_flag = false;
							   }
							   else {
								   show_message("Load palette first!", 7.0);
								   printf("Load palette first!\n");
							   }
// Scroll up - Przewi� w g�r�
				} else if ((cur_x > 38*TILE_SIZE) && (cur_x < 39*TILE_SIZE) && (cur_y < TILE_SIZE)
						&& (palette_drawn_flag == true) && (scroll_value > 0)) {
					--scroll_value;
					picker_mov = scroll_value;
					draw_palette(scroll_value);
					printf("Scroll up\n");
// Scroll down - Przewi� w d�
				} else if ((cur_x > 38*TILE_SIZE) && (cur_x < 39*TILE_SIZE) && (cur_y > 29*TILE_SIZE)
						&& (palette_drawn_flag == true) && ((palette_tiles_number - scroll_value) > 28)) {
					++scroll_value;
					picker_mov = scroll_value;
					draw_palette(scroll_value);
					printf("Scroll down\n");
// Info
				} else if ((cur_x > 34*TILE_SIZE) && (cur_x < 35*TILE_SIZE) && (cur_y > 28*TILE_SIZE) && (cur_y < 29*TILE_SIZE)) {
					// AKTUALNA WERSJA
					printf("Tile Map Editor version 0.11.2\n(C) 2012 Artur Wachnicki\n\n");
					al_show_native_message_box(display, "Info", "Tile Map Editor version 0.11.2", "(C) 2012 Artur Wachnicki", NULL, 0);
// Zmiana rozmiar�w mapy
				} else if ((cur_x > 15*TILE_SIZE) && (cur_x < 16*TILE_SIZE) && (cur_y > 28*TILE_SIZE) && (cur_y < 29*TILE_SIZE)) {
					resize_map();
// Prze��czanie warstw (layer�w) od 1 do 4
				} else if((cur_x > 27*TILE_SIZE) && (cur_x < 28*TILE_SIZE) && (cur_y > 28*TILE_SIZE) && (cur_y < 29*TILE_SIZE)) {
					++layer;
					if (layer > 4)
						layer = 1;
// Prze��czanie narz�dzi
				} else if ((cur_x > 13*TILE_SIZE) && (cur_x < 14*TILE_SIZE) && (cur_y > 28*TILE_SIZE) && (cur_y < 29*TILE_SIZE)) {
					//paint
					printf("Normal\n");
					draw_rectangle_flag = false;
					draw_line_flag = false;
				} else if ((cur_x > 17*TILE_SIZE) && (cur_x < 18*TILE_SIZE) && (cur_y > 28*TILE_SIZE) && (cur_y < 29*TILE_SIZE)) {
					//paint_line
					printf("Line\n");
					draw_rectangle_flag = false;
					draw_line_flag = true;
				} else if ((cur_x > 19*TILE_SIZE) && (cur_x < 20*TILE_SIZE) && (cur_y > 28*TILE_SIZE) && (cur_y < 29*TILE_SIZE)) {
					//paint_rectangle
					printf("Rectangle\n");
					draw_line_flag = false;
					draw_rectangle_flag = true;
				}
				break;
			case 2:
				printf("RMB\n");
// Podnoszenie kafelka prosto z mapy
				if ((cur_x < (DW*TILE_SIZE)) && (cur_y < (DH*TILE_SIZE)) &&
					(cur_x < (map_width*TILE_SIZE)) && (cur_y < (map_height*TILE_SIZE)) && (palette_drawn_flag == true)) {
						if (tab[(cur_y-mpos_y)/TILE_SIZE][(cur_x-mpos_x)/TILE_SIZE] >= 0)
							picker = tab[(cur_y-mpos_y)/TILE_SIZE][(cur_x-mpos_x)/TILE_SIZE];
						al_set_target_bitmap(current_tile);
						printf("%i\n", p_w_t);
						al_draw_bitmap_region(palette, (picker % p_w_t) * TILE_SIZE, (picker / p_w_t) * TILE_SIZE, TILE_SIZE, TILE_SIZE, 0, 0, 0);
						picker_flag = true;
						changes_flag = true;
				}
				break;
			}
		} else if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP){
// Przycisk myszy puszczony
			// 2-ga cz�� rysowania prostok�ta i linii.
			// Po puszczeniu przycisku myszy, pobierane s� wsp�rz�dne 2-go wierzcho�ka
			// i funkcja draw_rect rysuje odpowiedni prostok�t (albo draw_line linie).
			if (picker_flag && (cur_x < (DW*TILE_SIZE)) && (cur_y < (DH*TILE_SIZE)) &&
				(cur_x < (map_width * TILE_SIZE)) && (cur_y < (map_height * TILE_SIZE)) &&
				cur_x > 0 && cur_y > 0) {
					//-----------------------------------------------------//
					printf("%i\n", picker);
					rectx2 = cur_x/TILE_SIZE;
					recty2 = cur_y/TILE_SIZE;
					printf("x2: %3i, y2: %3i\n", rectx2, recty2);
					//-----------------------------------------------------//
					if (rectx1 >= 0 && recty1 >= 0) {
						if (draw_rectangle_flag) draw_rect(rectx1, recty1, rectx2, recty2, picker);
						else if (draw_line_flag) draw_line(rectx1, recty1, rectx2, recty2, picker);
					}
			}
			rectx1 = -20;	// Te dwie zmienne zn�w musz� by� < 0, �eby pod�wietlenie dobrze dzia�a�o
			recty1 = -20;
			changes_flag = true;
			painting = false;
			if (palette_drawn_flag)	// Wymusza przerysowanie mapy bezpo�rednio po puszczeniu przycisku.
				redraw_map();		// Bez tego widoczne jest denerwuj�ce migni�cie.
// ------------------------------------------ Keyboard input ------------------------------------------ //
		} else if (ev.type == ALLEGRO_EVENT_KEY_DOWN) {
			switch(ev.keyboard.keycode) {
// RIGHT
			case ALLEGRO_KEY_RIGHT:
				if ((map_width > DW) &&
					(mpos_x > ((DW-map_width)*TILE_SIZE)))
				mpos_x -= TILE_SIZE;
				break;
// LEFT
			case ALLEGRO_KEY_LEFT:
				if (mpos_x < 0)
				mpos_x += TILE_SIZE;
				break;
// DOWN
			case ALLEGRO_KEY_DOWN:
				if ((map_height > DH) &&
					(mpos_y > ((DH-map_height)*TILE_SIZE)))
				mpos_y -= TILE_SIZE;
				break;
// UP
			case ALLEGRO_KEY_UP:
				if (mpos_y < 0)
				mpos_y += TILE_SIZE;
				break;
// D
			case ALLEGRO_KEY_D:
				if ((palette_drawn_flag == true) && ((picker  < (palette_tiles_number - 1)))) {
					if (picker < picker_mov)
						picker = picker_mov;
					else
						++picker;
					al_set_target_bitmap(current_tile);
					al_draw_bitmap_region(palette, (picker % p_w_t) * TILE_SIZE, (picker / p_w_t) * TILE_SIZE, TILE_SIZE, TILE_SIZE, 0, 0, 0);
					picker_flag = true;
				}
				break;
// A
			case ALLEGRO_KEY_A:
				if ((picker > 0) && (palette_drawn_flag == true)) {
					if (picker > (picker_mov + 28))
						picker = (picker_mov + 27);
					else
						--picker;
					al_set_target_bitmap(current_tile);
					al_draw_bitmap_region(palette, (picker % p_w_t) * TILE_SIZE, (picker / p_w_t) * TILE_SIZE, TILE_SIZE, TILE_SIZE, 0, 0, 0);
					picker_flag = true;
				}
				break;
// W
			case ALLEGRO_KEY_W:
				if ((palette_drawn_flag == true) && (scroll_value > 0)) {
					--scroll_value;
					picker_mov = scroll_value;
					draw_palette(scroll_value);
					printf("Keyboard: scroll up\n");
				}
				break;
// S
			case ALLEGRO_KEY_S:
				if ((palette_drawn_flag == true) && ((palette_tiles_number - scroll_value) > 28)) {
					++scroll_value;
					picker_mov = scroll_value;
					draw_palette(scroll_value);
					printf("Keyboard: scroll down\n");
				}
				break;
			case ALLEGRO_KEY_T:
				if (name_change_flag == false)
					show_message(old_sname, 20.0);
				break;
// ESCAPE
			case ALLEGRO_KEY_ESCAPE:
				return 0;
			}
		}

		if (redraw && al_is_event_queue_empty(event_queue)) {
			redraw = 0;
			redrawal(cur_x, cur_y, picker, picker_flag, scroll_value, palette_drawn_flag,
					 palette_tiles_number, painting, draw_rectangle_flag, rectx1, recty1,
					 draw_line_flag);
		}
	}
// ---------------------------------------------------------------------------------- KONIEC P�TLI G��WNEJ JEST TUTAJ!!!

	destroy();
	PHYSFS_deinit();
	return 0;
}